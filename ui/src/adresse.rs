use std::sync::Arc;

use axum::{extract::Extension, handler::get, routing::BoxRoute, Json, Router};
use serde::{Deserialize, Serialize};
use sqlx::{query_as, FromRow};

use crate::State;

#[derive(Serialize, Deserialize, FromRow, Debug)]
pub(crate) struct Adresse {
    pub(crate) id: u32,
    pub(crate) stadt: String,
    pub(crate) strasse: String,
    pub(crate) hausnr: String,
    pub(crate) plz: u32,
}

async fn list_adresse(state: Extension<Arc<State>>) -> Json<Vec<Adresse>> {
    let state: Arc<State> = state.0;

    let adresse = query_as("SELECT * FROM adresse")
        .fetch_all(&state.db_pool)
        .await
        .unwrap();

    Json(adresse)
}

pub fn api() -> Router<BoxRoute> {
    Router::new().route("/", get(list_adresse)).boxed()
}
