use axum::{
    extract::{self, Extension, Json},
    handler::get,
    routing::BoxRoute,
    Router,
};
use serde::{Deserialize, Serialize};
use sqlx::{query_as, types::time::Date, FromRow};

use std::sync::Arc;

use crate::{adresse::Adresse, bestellung::Bestellung, State};

#[derive(Serialize, Deserialize, FromRow, Debug)]
pub(crate) struct Kunde {
    pub(crate) id: u64,
    pub(crate) name: String,
    pub(crate) vorname: String,
    pub(crate) geburtsdatum: Date,
    pub(crate) telefon: String,
    pub(crate) email: String,
    pub(crate) adresse_id: u64,
}

async fn list_kunden(state: Extension<Arc<State>>) -> Json<Vec<Kunde>> {
    let state: Arc<State> = state.0;

    let kunden = query_as("SELECT * FROM kunde")
        .fetch_all(&state.db_pool)
        .await
        .unwrap();

    Json(kunden)
}

async fn show_kunde(state: Extension<Arc<State>>, id: extract::Path<u32>) -> Json<Kunde> {
    let state: Arc<State> = state.0;
    let id: u32 = id.0;

    let kunden = query_as("SELECT * FROM kunde WHERE id = ?")
        .bind(id)
        .fetch_one(&state.db_pool)
        .await
        .unwrap();

    Json(kunden)
}

async fn show_adresse_of_kunde(
    state: Extension<Arc<State>>,
    id: extract::Path<u64>,
) -> Json<Adresse> {
    let state: Arc<State> = state.0;
    let id: u64 = id.0;

    let kunden = query_as(
        r#"
SELECT adresse.* FROM kunde
JOIN adresse ON kunde.adresse_id = adresse.id
WHERE kunde.id = ?
"#,
    )
    .bind(id)
    .fetch_one(&state.db_pool)
    .await
    .unwrap();

    Json(kunden)
}

async fn show_bestellung_of_kunde(
    state: Extension<Arc<State>>,
    id: extract::Path<u64>,
) -> Json<Vec<Bestellung>> {
    let state: Arc<State> = state.0;
    let id: u64 = id.0;

    let kunden = query_as(
        r#"
SELECT bestellung.* FROM kunde
JOIN bestellung ON bestellung.kunde_id = kunde.id
WHERE kunde.id = ?
"#,
    )
    .bind(id)
    .fetch_all(&state.db_pool)
    .await
    .unwrap();

    Json(kunden)
}

pub fn api() -> Router<BoxRoute> {
    Router::new()
        .route("/", get(list_kunden))
        .route("/:id", get(show_kunde))
        .route("/:id/adresse", get(show_adresse_of_kunde))
        .route("/:id/bestellung", get(show_bestellung_of_kunde))
        .boxed()
}
