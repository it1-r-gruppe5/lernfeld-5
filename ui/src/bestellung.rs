use std::sync::Arc;

use axum::{
    extract::{Extension, Json},
    handler::get,
    routing::BoxRoute,
    Router,
};
use serde::{Deserialize, Serialize};
use sqlx::{query_as, types::time::Date, FromRow};

use crate::State;

#[derive(Serialize, Deserialize, FromRow, Debug)]
pub(crate) struct Bestellung {
    pub(crate) id: u32,
    pub(crate) gesamtpreis: f64,
    pub(crate) datum: Date,
    pub(crate) kunde_id: u32,
}

async fn list_bestellung(state: Extension<Arc<State>>) -> Json<Vec<Bestellung>> {
    let state: Arc<State> = state.0;

    let bestellung = query_as("SELECT * FROM bestellung")
        .fetch_all(&state.db_pool)
        .await
        .unwrap();

    Json(bestellung)
}

pub fn api() -> Router<BoxRoute> {
    Router::new().route("/", get(list_bestellung)).boxed()
}
