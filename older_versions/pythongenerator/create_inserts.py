# -*- coding: utf-8 -*-
import random

ernaehrungskategorie = {
    "Vegan": "Keine tierischen Produkte",
    "Vegetarisch": "Kein Fleisch",
    "Pesecetarisch": "Nur Fisch",
    "Frutarisch": "Nur was die Pflanzen hergeben",
    "Low Carb": "Wenig Fett",
    "High Protein": "Viel Eiweiß",
}

mengeneinhit = ["Kilogramm", "Liter", "Stück"]

zutaten = {
    "Bandnudeln": {
        "name": "Bandnudeln",
        "bestand": 20,
        "nettopreis": 0.99,
        "lieferant_id": 1,
        "kohlenhydrate": 68.0,
        "kalorien": 364,
        
        "fett": 4.0,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Zwiebel": {
        "name": "Zwiebel",
        "bestand": 150,
        "nettopreis": 0.49,
        "lieferant_id": 1,
        "kohlenhydrate": 6.0,
        "kalorien": 33,
        
        "fett": 0.0,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Öl": {
        "name": "Öl",
        "bestand": 50,
        "nettopreis": 2.0,
        "lieferant_id": 2,
        "kohlenhydrate": 0.0,
        "kalorien": 884,
        
        "fett": 100.0,
        "mengeneinheit": "Liter",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Hackfleisch": {
        "name": "Hackfleisch",
        "bestand": 20,
        "nettopreis": 6.0,
        "lieferant_id": 1,
        "kohlenhydrate": 0.0,
        "kalorien": 336,
        
        "fett": 28.0,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Rotwein": {
        "name": "Rotwein",
        "bestand": 30,
        "nettopreis": 6.50,
        "lieferant_id": 2,
        "kohlenhydrate": 2.61,
        "kalorien": 85,
        
        "fett": 0.0,
        "mengeneinheit": "Liter",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Tomatenmark": {
        "name": "Tomatenmark",
        "bestand": 40,
        "nettopreis": 0.69,
        "lieferant_id": 1,
        "kohlenhydrate": 12.9,
        "kalorien": 74,
        
        "fett": 0.2,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Thymian": {
        "name": "Thymian",
        "bestand": 20,
        "nettopreis": 4.99,
        "lieferant_id": 2,
        "kohlenhydrate": 7.35,
        "kalorien": 52,
        
        "fett": 1.2,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Cayennepfeffer": {
        "name": "Cayennepfeffer",
        "bestand": 20,
        "nettopreis": 5.99,
        "lieferant_id": 2,
        "kohlenhydrate": 57.0,
        "kalorien": 318,
        
        "fett": 17,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Salz": {
        "name": "Salz",
        "bestand": 20,
        "nettopreis": 0.89,
        "lieferant_id": 2,
        "kohlenhydrate": 0.0,
        "kalorien": 0,
        
        "fett": 0.0,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Gouda": {
        "name": "Gouda",
        "bestand": 10,
        "nettopreis": 4.99,
        "lieferant_id": 3,
        "kohlenhydrate": 2.2,
        "kalorien": 356,
        
        "fett": 22.3,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Sahne": {
        "name": "Sahne",
        "bestand": 50,
        "nettopreis": 0.99,
        "lieferant_id": 3,
        "kohlenhydrate": 3.7,
        "kalorien": 196,
        
        "fett": 19.0,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Ei": {
        "name": "Ei",
        "bestand": 150,
        "nettopreis": 0.30,
        "lieferant_id": 3,
        "kohlenhydrate": 0.4,
        "kalorien": 89,
        
        "fett": 6.5,
        "mengeneinheit": "Stück",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Mehl": {
        "name": "Mehl",
        "bestand": 45,
        "nettopreis": 1.20,
        "lieferant_id": 1,
        "kohlenhydrate": 76.0,
        "kalorien": 364,
        
        "fett": 1.0,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Milch": {
        "name": "Milch",
        "bestand": 50,
        "nettopreis": 1.30,
        "lieferant_id": 3,
        "kohlenhydrate": 5.0,
        "kalorien": 42,
        
        "fett": 1.0,
        "mengeneinheit": "Liter",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Zucker": {
        "name": "Zucker",
        "bestand": 48,
        "nettopreis": 1.20,
        "lieferant_id": 2,
        "kohlenhydrate": 100.0,
        "kalorien": 387,
        
        "fett": 0.0,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Champignons": {
        "name": "Champignons",
        "bestand": 10000,
        "nettopreis": 2.0,
        "lieferant_id": 2,
        "kohlenhydrate": 3.3,
        "kalorien": 22,
        
        "fett": 0.3,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Zuccini": {
        "name": "Zuccini",
        "bestand": 100,
        "nettopreis": 1.50,
        "lieferant_id": 2,
        "kohlenhydrate": 2.2,
        "kalorien": 19,
        
        "fett": 0.4,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Mais": {
        "name": "Mais",
        "bestand": 71,
        "nettopreis": 1.15,
        "lieferant_id": 1,
        "kohlenhydrate": 18.0,
        "kalorien": 86,
        
        "fett": 0.0,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Knoblauch": {
        "name": "Knoblauch",
        "bestand": 54,
        "nettopreis": 1.59,
        "lieferant_id": 1,
        "kohlenhydrate": 28.4,
        "kalorien": 145,
        
        "fett": 0.1,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
    "Dill": {
        "name": "Dill",
        "bestand": 2000,
        "nettopreis": 0.1,
        "lieferant_id": 1,
        "kohlenhydrate": 8.0,
        "kalorien": 55,
        
        "fett": 0.8,
        "mengeneinheit": "Kilogramm",
        "proteine": 0.0,
        "zucker": 0.0,
        "gesaettigte_fettsaeuren": 0.0,
        "ballaststoffe": 0.0,
        "natrium": 0.0,
    },
}


def create_insert_zutaten(zutaten):
    for zutat in zutaten.values():
        print(create_insert_zutat(zutat))


def create_insert_zutat(rezept):
    return f"""INSERT INTO zutat
    SET name = '{rezept['name']}',
        bestand = {rezept['bestand']},
        nettopreis = {rezept['nettopreis']},
        kohlenhydrate = {rezept['kohlenhydrate']},
        kalorien = {rezept['kalorien']},
        fett = {rezept['fett']},
        proteine = {rezept['proteine']},
        zucker = {rezept['zucker']},
        gesaettigte_fettsaeuren = {rezept['gesaettigte_fettsaeuren']},
        ballaststoffe = {rezept['ballaststoffe']},
        natrium = {rezept['natrium']},

        mengeneinheit_id = (
            SELECT id
                FROM mengeneinheit
                WHERE name = '{rezept['mengeneinheit']}');"""


# def create_insert_zutat_rezeptmap(rezept):
# #menge
# | #rezept_id
# | #zutat_id
#     return f"""INSERT INTO rezept_zutat
#     SET name = '{rezept['name']}',
#         bestand = {rezept['bestand']},
#         nettopreis = {rezept['nettopreis']},
#         kohlenhydrate = {rezept['kohlenhydrate']},
#         kalorien = {rezept['kalorien']},
#         fett = {rezept['fett']},
#         proteine = {rezept['proteine']},
#         zucker = {rezept['zucker']},
#         gesaettigte_fettsaeuren = {rezept['gesaettigte_fettsaeuren']},
#         ballaststoffe = {rezept['ballaststoffe']},
#         natrium = {rezept['natrium']},

#         mengeneinheit_id = (
#             SELECT id
#                 FROM mengeneinheit
#                 WHERE name = '{rezept['mengeneinheit']}');"""

def insertRecipes(num):
    for i in range(num):
        print(insertRecipe(i))


def insertRecipe(currentnum):
    i = currentnum
    return f"""INSERT INTO rezept
    SET
        name = 'Rezept {i}',
        beschreibung = '{"Lorem ipsum..."}',
        zubereitung = '{"Lorem ipsum..."}';"""


def gen_rnd_recipe(rezNum):
    z = random.sample(zutaten.keys(), 5)
    res = ""
    for zut in z:
        res += f"""
INSERT INTO rezept_zutat
SET
    menge = {random.randint(1,100)},
    rezept_id = {rezNum},
    zutat_id = (
        SELECT id
            FROM zutat
            WHERE name = '{zut}');"""
    return res


def ernaehrungskategorie_für_rezept(rezept_id, ernaehrungskategorien):
    kategorien = random.sample(ernaehrungskategorien, 2)

    return f"""
INSERT INTO rezept_ernaehrungskategorie
SET
    ernaehrungskategorie_id = {kategorien[0]},
    rezept_id = {rezept_id};
INSERT INTO rezept_ernaehrungskategorie
SET
    ernaehrungskategorie_id = {kategorien[1]},
    rezept_id = {rezept_id};"""


def supplyforingredients(zutnr):
    lief_ids = random.sample(range(1, 4), 2)
    res = ""
    for id in lief_ids:
        res += f"""
INSERT INTO lieferant_zutat
SET
    lieferant_id = {id},
    zutat_id = {zutnr},
    menge_pro_woche = {round(random.uniform(1.0,10.0),2)};"""
    return res


def mappingRestrictions(ingr_nr):
    ingr = random.sample(range(1, 6), 2)
    res = ""
    for i in ingr:
        res += f"""
INSERT INTO zutat_beschraenkung
SET
    zutat_id = {ingr_nr},
    beschraenkung_id = {i};"""
    return res


if __name__ == "__main__":
    # create_insert_zutaten(zutaten)
    # insertRecipes(5)
    # for i in range(1, 6):
    #     print(gen_rnd_recipe(i), end="")
    for i in range(1, 6):
        print(ernaehrungskategorie_für_rezept(i, [x for x in range(1, 7)]))
