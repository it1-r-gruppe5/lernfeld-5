INSERT INTO RezeptPosition (FK_Rezept,FK_Zutat,Menge,FK_Mengeneinheit) VALUES
(1, 1, 0.2, 1),
(1, 2, 1, 3),
(1, 3, 0.02, 2),
(1, 4, 0.25, 1),
(1, 5, 0.125, 2),
(1, 6, 0.05, 2),
(1, 7, 0.025, 1),
(1, 8, 0.025, 1),
(1, 9, 0.075, 1),
(1, 10, 0.1, 1),
(1, 11, 0.2, 1),
(2, 16, 12, 3),
(2, 17, 1, 3),
(2, 18, 1, 3),
(2, 2, 1, 3),
(2, 19, 3, 3),
(2, 20, 0.1, 1),
(2, 9, 0.01, 1),
(2, 3, 0.02, 2),
(3, 12, 3, 3),
(3, 14, 0.75, 2),
(3, 13, 0.4, 1),
(3, 9, 0.01, 1);