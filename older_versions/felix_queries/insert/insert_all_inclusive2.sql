INSERT INTO Kunde (Vorname, Nachname, Geburtsdatum) VALUES 
('Kira','Wellensteyn','1990-05-05'),
('Dorothea','Foede','2000-03-24'),
('Sigrid','Leberer','1989-09-21'),
('Hanna','Soerensen','1974-04-03'),
('Marten','Schnitter','1964-04-17'),
('Belinda','Maurer','1978-09-09'),
('Armin','Gessert','1978-01-29'),
('Jean-Marc','Haessig','1982-08-30'),
('Eric','Urocki','1999-12-04');

INSERT INTO Lieferant (Name) VALUES 
('Bio-Hof Müller'),
('Obst-Hof Altes Land'),
('Molkerei Henning');

INSERT INTO Adresse(Strasse, HausNr, PLZ, Stadt, FK_Kunde, FK_Lieferant) VALUES 
('Eppendorfer Landstrasse', '104', '20249','Hamburg',1 ,1),
('Ohmstraße', '23', '22765','Hamburg',2 ,1),
('Bilser Berg', '6', '20459','Hamburg',3 ,1),
('Alter Teichweg', '95', '22049','Hamburg',4 ,2),
('Stübels', '10', '22935','Barsbüttel',5 ,2),
('Grotelertwiete', '4a', '21075','Hamburg',6 ,2),
('Küstersweg', '3', '21079','Hamburg',7 ,3),
('Neugrabener Bahnhofstraße', '30', '21149','Hamburg',8 ,3),
('Elbchaussee', '228', '22605','Hamburg',9 ,3);

INSERT INTO Rezept (Name, Beschreibung) VALUES 
('Straßburger Auflauf','Ein leckerer Nudelauflauf'),
('Champignon-Zucchini-Pfanne','Leichtes veganes Abendessen'),
('Pfannkuchen','Ein leckeres Frühstück');

INSERT INTO Mengeneinheit(Bezeichnung) VALUES 
('Kilogramm'),
('Liter'),
('Stück');

INSERT INTO Zutat(Name, Bestand, FK_Mengeneinheit, Nettopreis, FK_Lieferant, Kohlenhydrate, Kalorien, Eiweiss, Fett) VALUES 
('Bandnudeln', 20,2, 0.99, 1, 68.0, 364 ,14.0, 4.0),
('Zwiebel', 150 , 4 , 0.49, 1,6.0 , 33 ,1.0, 0.0),
('Öl', 50 , 4 , 2.0, 2, 0.0, 884 ,0.0, 100.0),
('Hackfleisch', 20 , 2 , 6.0, 1,0.0 , 336 ,21.0, 28.0),
('Rotwein', 30 , 3, 6.50, 2, 2.61, 85 ,0.07, 0.0),
('Tomatenmark', 40 ,4, 0.69, 1, 12.9, 74 ,4.5, 0.2),
('Thymian', 20 ,4, 4.99, 2, 7.35, 52 ,1.48, 1.2),
('Cayennepfeffer', 20 ,4, 5.99, 2, 57.0, 318 ,12.0, 17),
('Salz', 20 ,4, 0.89, 2, 0.0, 0 ,0.0, 0.0),
('Gouda', 10 , 2, 4.99, 3, 2.2, 356 ,24.7, 22.3),
('Sahne', 50 ,4, 0.99, 3, 3.7, 196 ,2.7, 19),
('Ei', 150 ,4, 0.30, 3, 0.4, 89 , 7.5, 6.5),
('Mehl', 45 , 4, 1.20, 1, 76.0 , 364 ,10.0, 1.0),
('Milch', 50 , 4, 1.30, 3, 5.0, 42 ,3.4, 1.0),
('Zucker', 48 ,4, 1.20, 2, 100.0, 387 ,0.0, 0.0),
('Champignons', 10000 , 1, 2.0, 2,3.3 , 22 ,3.1, 0.3),
('Zuccini', 100 ,4, 1.50, 2, 2.2, 19 ,1.6, 0.4),
('Mais', 71 ,4, 1.15, 1, 18.0, 86, 2.0, 0.0),
('Knoblauch', 54 , 4, 1.59, 1, 28.4, 145, 6.0, 0.1),
('Dill', 2000 ,1, 0.1, 1, 8.0, 55, 3.7, 0.8);

INSERT INTO Bestellung (Gesamtpreis,Bestelldatum,FK_Kunde) VALUES
(24.72,'12.10.2021',1),
(10.62,'12.10.2021',5),
(4.29,'12.10.2021',7);

INSERT INTO Ernaehrungskategorie(Bezeichnung, Beschreibung) values 
('Vegan','Keine tierischen Produkte'),
('Vegetarisch','Kein Fleisch'),
('Pesecetarisch','Nur Fisch'),
('Frutarisch','Nur was die Pflanzen hergeben'),
('Low Carb','Wenig Fett'),
('High Protein','Viel Eiweiß');

INSERT INTO Beschraenkung(Bezeichnung, Beschreibung) values 
('Gluten','Getreide Unverträglichkeit'),
('Lactose','Keine Milchprodukte'),
('Fructose','Kein Fruchtzucker'),
('Erdnuss','Keine Erdnüsse'),
('Schalenfrüchte','Keine Haselnüsse, Wallnüsse, Macadamianüsse, Parranüsse, Pecannüsse, Cashewnüsse, Pistazien'),
('Ei','Keine Eier');

INSERT INTO MAP_Rezept_Beschraenkung(FK_Rezept,FK_Beschraenkung) VALUES 
(1,1),
(1,2),
(3,1),
(3,2),
(3,6);

INSERT INTO MAP_Rezept_Ernaehrungskategorie (FK_Rezept,FK_Ernaehrungskategorie) VALUES 
(1,6),
(2,1),
(2,5),
(3,2);

INSERT INTO Bestellposition (FK_Bestellung, FK_Zutat, Menge) VALUES
(1,1,0.2),
(1,2,1),
(1,3,1),
(1,4,0.25),
(1,5,1),
(1,6,1),
(1,7,1),
(1,8,1),
(1,9,1),
(1,10,0.1),
(1,11,1),
(2,16,1),
(2,17,1),
(2,18,1),
(2,2,1),
(2,19,1),
(2,20,1),
(2,9,1),
(2,3,1),
(3,12,3),
(3,14,1),
(3,13,1),
(3,9,1);

INSERT INTO RezeptPosition (FK_Rezept,FK_Zutat,Menge,FK_Mengeneinheit) VALUES
(1, 1, 0.2, 1),
(1, 2, 1, 3),
(1, 3, 0.02, 2),
(1, 4, 0.25, 1),
(1, 5, 0.125, 2),
(1, 6, 0.05, 2),
(1, 7, 0.025, 1),
(1, 8, 0.025, 1),
(1, 9, 0.075, 1),
(1, 10, 0.1, 1),
(1, 11, 0.2, 1),
(2, 16, 12, 3),
(2, 17, 1, 3),
(2, 18, 1, 3),
(2, 2, 1, 3),
(2, 19, 3, 3),
(2, 20, 0.1, 1),
(2, 9, 0.01, 1),
(2, 3, 0.02, 2),
(3, 12, 3, 3),
(3, 14, 0.75, 2),
(3, 13, 0.4, 1),
(3, 9, 0.01, 1);