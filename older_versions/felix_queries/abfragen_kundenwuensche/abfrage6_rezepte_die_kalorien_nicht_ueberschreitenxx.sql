select AVG(tZutat.Kalorien) AS [Kalorien],
tKunde.Vorname,
tKunde.Nachname,
AVG(tZutat.Eiweiss) AS [Eiweiss],
AVG(tZutat.Kohlenhydrate) AS [Kohlenhydrate],
AVG(tZutat.Fett) AS [Fett]
from Bestellposition AS [tBestellposition]
join Bestellung as [tBestellung] on tBestellung.ID = tBestellposition.FK_Bestellung
join Kunde as [tKunde] on tKunde.ID = tBestellung.FK_Kunde
join Zutat as [tZutat] on tZutat.ID = tBestellposition.FK_Zutat
WHERE tKunde.Nachname = 'Schnitter'
GROUP BY tBestellposition.FK_Bestellung, tKunde.Vorname, tKunde.Nachname