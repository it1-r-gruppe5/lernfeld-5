SELECT Rezept.Name, Rezept.Beschreibung, Ernaehrungskategorie.Bezeichnung
FROM Rezept, Ernaehrungskategorie
WHERE ((
SELECT COUNT(*)
FROM RezeptPosition
WHERE RezeptPosition.FK_Rezept = Rezept.ID 
)) < '5' AND Ernaehrungskategorie.Bezeichnung = 'Vegan'
