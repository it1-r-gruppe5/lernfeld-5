SELECT Zutat.Name
FROM RezeptPosition
INNER JOIN Rezept ON Rezept.ID = RezeptPosition.FK_Rezept
INNER JOIN Zutat ON Zutat.ID = RezeptPosition.FK_Zutat
WHERE Rezept.Name = 'Pfannkuchen'
