select AVG(Zutat.Kalorien) AS [Kalorien],
Kunde.Vorname,
Kunde.Nachname,
AVG(Zutat.Eiweiss) AS [Eiweiss],
AVG(Zutat.Kohlenhydrate) AS [Kohlenhydrate],
AVG(Zutat.Fett) AS [Fett]
from Bestellposition 
join Bestellung on Bestellung.ID = Bestellposition.FK_Bestellung
join Kunde on Kunde.ID = Bestellung.FK_Kunde
join Zutat on Zutat.ID = Bestellposition.FK_Zutat
WHERE Kunde.Nachname = 'Schnitter'
GROUP BY Bestellposition.FK_Bestellung, Kunde.Vorname, Kunde.Nachname