from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
from polls.models import RezeptZutat
from polls.models import Rezept
def index(request):
    latest_question_list = Rezept.objects.all()
    context = {'latest_question_list': latest_question_list}
    return render(request, 'recipe/index.html', context)


def detail(request, recipe_id):
    ks = ['name',
        'ballaststoffe',
     'bestand',
     'gesaettigte_fettsaeuren',
     'kalorien',
     'kohlenhydrate',
     'mengeneinheit',
     'name',
     'natrium',
     'nettopreis',
     'zucker']
    recipe_data = RezeptZutat.objects.select_related('rezept', 'zutat').filter(rezept_id = recipe_id)
    return render(request, 'recipe/detail.html', {'recipe_data': recipe_data, 'keys': ks})
