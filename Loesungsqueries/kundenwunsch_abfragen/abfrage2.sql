select rezept.name
from rezept_ernaehrungskategorie
join rezept on rezept_ernaehrungskategorie.rezept_id = rezept.id
join ernaehrungskategorie on rezept_ernaehrungskategorie.ernaehrungskategorie_id = ernaehrungskategorie.id
where ernaehrungskategorie.name = 'Vegan'