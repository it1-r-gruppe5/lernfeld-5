-- get user information (DSGVO)

SELECT kunde.id AS KundeID, 
kunde.name AS KundeNachname,
kunde.vorname as KundeVorname,
kunde.telefon AS KundeTelefonNr,
kunde.email as KundeEmailAdr,
bestellung.id AS BestellungID,
bestellung.datum AS BestellDatum,
bestellung.gesamtpreis AS BetellungGesamtpreis,
adresse.id AS AdressID, 
adresse.strasse AS Strasse,
adresse.hausnr AS HausNr,
adresse.plz AS PLZ,
adresse.stadt AS Stadt
FROM kunde
JOIN bestellung on bestellung.kunde_id = kunde.id
JOIN adresse on kunde.adresse_id = adresse.id
WHERE kunde.vorname = 'Kira';
